#include "../include/GUI_Line_Object.hpp"

namespace catan_game {
GUI_Line_Object::GUI_Line_Object(const sf::Vector2f &point1,
                                 const sf::Vector2f &point2, sf::Color color,
                                 Context *context)
    : m_color(color), m_thickness(10.f), m_context(context) {
  sf::Vector2f direction = point2 - point1;
  sf::Vector2f unitDirection = direction / std::sqrt(direction.x * direction.x +
                                                     direction.y * direction.y);
  sf::Vector2f unitPerpendicular(-unitDirection.y, unitDirection.x);

  sf::Vector2f offset = (m_thickness / 2.f) * unitPerpendicular;

  m_vertices[0].position = point1 + offset;
  m_vertices[1].position = point2 + offset;
  m_vertices[2].position = point2 - offset;
  m_vertices[3].position = point1 - offset;

  for (int i = 0; i < 4; ++i) {
    m_vertices[i].color = m_color;
  }
}

void GUI_Line_Object::draw() const {
  m_context->m_window->draw(m_vertices, 4, sf::Quads);
}

} // namespace catan_game
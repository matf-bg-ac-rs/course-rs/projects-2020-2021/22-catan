#include <SFML/Graphics/CircleShape.hpp>

#include "../include/Game.hpp"
#include "../include/Loading.hpp"

namespace catan_game {

/**************** Constructors/Destructor/AssignmentOperator ****************/

Game::Game() {
  m_context = new Context();
  m_context->m_window->create(sf::VideoMode(WIDTH, HEIGHT), "CATAN",
                              sf::Style::Close);
  m_context->m_states->add(new Loading(m_context));
}

Game::~Game() { delete m_context; }

/**************** Other Methods ****************/

void Game::run() {
  sf::Clock clock;
  sf::Time time_since_last_frame = sf::Time::Zero;

  while (m_context->m_window->isOpen()) {
    time_since_last_frame += clock.restart();

    while (time_since_last_frame > TIME_PER_FRAME) {
      time_since_last_frame -= TIME_PER_FRAME;

      m_context->m_states->process_state_change();
      m_context->m_states->get_current()->process_input();
      m_context->m_states->get_current()->update(TIME_PER_FRAME);
      m_context->m_states->get_current()->draw();
    }
  }
}

} // namespace catan_game
#include "../include/GUI_Robber.hpp"

namespace catan_game {
/**************** Constructors/Destructor/AssignmentOperator ****************/

GUI_Robber::GUI_Robber(float center_x, float center_y, Texture_ID texture_id,
                       Context *context)
    : GUI_Sprite_Object(center_x, center_y,
                        context->m_assets->get_texture(texture_id), context) {}

} // namespace catan_game
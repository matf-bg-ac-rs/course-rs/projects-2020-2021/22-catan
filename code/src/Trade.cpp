#include "../include/Trade.hpp"

bool catan_game::Trade::is_trade_clicked(sf::Vector2f mouse) {
  sf::Vector2f pos = m_trade_button.getPosition();
  bool hor = pos.x < mouse.x && mouse.x < pos.x + 150;
  bool ver = pos.y < mouse.y && mouse.y < pos.y + 50;
  if (!hor || !ver) {
    return false;
  }
  return true;
}

bool catan_game::Trade::is_trade_reset_clicked(sf::Vector2f mouse) {
  sf::Vector2f pos = m_trade_reset.getPosition();
  bool hor = pos.x < mouse.x && mouse.x < pos.x + 150;
  bool ver = pos.y < mouse.y && mouse.y < pos.y + 50;
  if (!hor || !ver) {
    return false;
  }
  return true;
}

void catan_game::Trade::draw(std::vector<Player *> players) {
  m_trade_reset =
      sf::Text("RESET TRADE VALUES", m_context->m_assets->get_font(1));
  m_trade_reset.setFillColor(sf::Color::Yellow);
  m_trade_reset.setPosition(250, 650);
  m_context->m_window->draw(m_trade_reset);
  m_trade_button =
      sf::Text("TRADE WITH: " +
                   players[m_gui_players->GetMPlayerToTradeWith()]->get_name(),
               m_context->m_assets->get_font(1));
  m_trade_button.setFillColor(
      players[m_gui_players->GetMPlayerToTradeWith()]->get_color());
  m_trade_button.setPosition(30, 650);
  m_context->m_window->draw(m_trade_button);
}

void catan_game::Trade::trade(int m_turn) {
  auto players = m_context->m_assets->get_players();
  auto resources_clicked = m_gui_players->GetMTradeValues();
  int resource_amount_take = -1;
  int resource_type_take = -1;
  int resource_amount_give = -1;
  int resource_type_give = -1;
  int player_id = -1;

  if (m_gui_players->GetMPlayerToTradeWith() == m_turn) {
    m_gui_players->clear_trade_value();
    std::cout << "You can't trade with yourself" << std::endl;
    return;
  }
  for (int player = 1; player < 5; ++player) {
    if (player != m_turn) {
      if (player == m_gui_players->GetMPlayerToTradeWith()) {
        for (int resource = 0; resource < 5; ++resource) {
          auto card = static_cast<ResourceType>(resource);
          if (resources_clicked[player][card] > 0) {
            resource_amount_take = resources_clicked[player][card];
            resource_type_take = resource;
            player_id = player;
          }
        }
      }
    } else {
      for (int resource = 0; resource < 5; ++resource) {
        auto card = static_cast<ResourceType>(resource);
        if (resources_clicked[player][card] > 0) {
          resource_amount_give = resources_clicked[player][card];
          resource_type_give = resource;
        }
      }
    }
  }
  if (resource_amount_take == -1 || resource_amount_give == -1 ||
      resource_type_take == -1 || resource_type_give == -1 || player_id == -1) {
    std::cout << "Not a valid trade" << std::endl;
    m_gui_players->clear_trade_value();
    return;
  }
  players[m_turn]->trade(static_cast<ResourceType>(resource_type_give),
                         resource_amount_give,
                         static_cast<ResourceType>(resource_type_take),
                         resource_amount_take, *players[player_id]);
  m_gui_players->clear_trade_value();
}

catan_game::Trade::Trade(Context *context, GUI_Players *gui_players)
    : m_context(context), m_gui_players(gui_players) {}

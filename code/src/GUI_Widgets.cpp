#include "../include/GUI_Widgets.hpp"

namespace catan_game {
GUI_Widgets::GUI_Widgets(Context *context, GameState *game_state)
    : m_context(context), m_game_state(game_state) {
  m_font = m_context->m_assets->get_font(1);

  initialise_shop();
  initialise_dices();

  m_end_move = sf::Text("END MOVE", m_font);
  m_end_move.setFillColor(sf::Color::Red);
  m_end_move.setPosition(800, 650);

  m_dices = sf::Text("DICES", m_font);
  m_dices.setFillColor(sf::Color::Yellow);
  m_dices.setPosition(700, 650);
}

GUI_Widgets::~GUI_Widgets() {}

void GUI_Widgets::initialise_shop() {
  if (!m_texture_road.loadFromFile("../resources/xd_exports/PlainRoad.png")) {
    std::cout << __FILE__ << " "
              << "Failed loading plain road" << std::endl;
    exit(EXIT_FAILURE);
  }
  m_plain_road.setTexture(m_texture_road);
  m_plain_road.setPosition(700, 530);
  m_plain_road.setScale(0.4, 0.4);
  if (!m_texture_house.loadFromFile("../resources/xd_exports/PlainHouse.png")) {
    std::cout << __FILE__ << " "
              << "Failed loading plain house" << std::endl;
    exit(EXIT_FAILURE);
  }
  m_plain_house.setTexture(m_texture_house);
  m_plain_house.setPosition(770, 530);
  m_plain_house.setScale(0.4, 0.4);

  if (!m_texture_city.loadFromFile("../resources/xd_exports/PlainCity.png")) {
    std::cout << __FILE__ << " "
              << "Failed loading plain city" << std::endl;
    exit(EXIT_FAILURE);
  }
  m_plain_city.setTexture(m_texture_city);
  m_plain_city.setPosition(840, 530);
  m_plain_city.setScale(0.4, 0.4);
}

void GUI_Widgets::initialise_dices() {
  if (!m_texture_one.loadFromFile("../resources/xd_exports/Dice1.png")) {
    std::cout << __FILE__ << " "
              << "Failed loading dice 1" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (!m_texture_two.loadFromFile("../resources/xd_exports/Dice2.png")) {
    std::cout << __FILE__ << " "
              << "Failed loading dice 2" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (!m_texture_three.loadFromFile("../resources/xd_exports/Dice3.png")) {
    std::cout << __FILE__ << " "
              << "Failed loading dice 3" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (!m_texture_four.loadFromFile("../resources/xd_exports/Dice4.png")) {
    std::cout << __FILE__ << " "
              << "Failed loading dice 4" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (!m_texture_five.loadFromFile("../resources/xd_exports/Dice5.png")) {
    std::cout << __FILE__ << " "
              << "Failed loading dice 5" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (!m_texture_six.loadFromFile("../resources/xd_exports/Dice6.png")) {
    std::cout << __FILE__ << " "
              << "Failed loading dice 6" << std::endl;
    exit(EXIT_FAILURE);
  }
}

void GUI_Widgets::draw(const std::basic_string<char> player_name,
                       sf::Color color) {
  update_on_turn(player_name, color);
  set_dices();
  m_context->m_window->draw(m_turn);
  m_context->m_window->draw(m_dices);
  m_context->m_window->draw(m_first_dice);
  m_context->m_window->draw(m_second_dice);
  m_context->m_window->draw(m_end_move);
  m_context->m_window->draw(m_plain_road);
  m_context->m_window->draw(m_plain_house);
  m_context->m_window->draw(m_plain_city);
}

void GUI_Widgets::update_on_turn(const std::basic_string<char> player_name,
                                 sf::Color color) {
  m_turn = sf::Text("TURN: " + player_name, m_font);
  m_turn.setFillColor(color);
  m_turn.setPosition(30, 20);
}

bool GUI_Widgets::is_dices_clicked(sf::Vector2f &mouse) const {
  sf::Vector2f pos = m_dices.getPosition();
  bool hor = pos.x < mouse.x && mouse.x < pos.x + 100;
  bool ver = pos.y < mouse.y && mouse.y < pos.y + 50;
  if (!hor || !ver) {
    return false;
  }
  return true;
}

bool GUI_Widgets::is_end_move_clicked(sf::Vector2f &mouse) const {
  sf::Vector2f pos = m_end_move.getPosition();
  bool hor = pos.x < mouse.x && mouse.x < pos.x + 150;
  bool ver = pos.y < mouse.y && mouse.y < pos.y + 50;
  if (!hor || !ver) {
    return false;
  }
  return true;
}

bool GUI_Widgets::is_plain_house_clicked(sf::Vector2f &mouse) const {
  sf::FloatRect f_bounds = m_plain_house.getGlobalBounds();
  if (f_bounds.contains(mouse)) {
    return true;
  } else {
    return false;
  }
}

bool GUI_Widgets::is_plain_city_clicked(sf::Vector2f &mouse) const {
  sf::FloatRect f_bounds = m_plain_city.getGlobalBounds();
  if (f_bounds.contains(mouse)) {
    return true;
  } else {
    return false;
  }
}

bool GUI_Widgets::is_plain_road_clicked(sf::Vector2f &mouse) const {
  sf::FloatRect f_bounds = m_plain_road.getGlobalBounds();
  if (f_bounds.contains(mouse)) {
    return true;
  } else {
    return false;
  }
}

void GUI_Widgets::set_dices() {
  auto dices = m_game_state->get_board()->get_dices();
  if (dices.first == 0) {
    return;
  }

  if (dices.first == 1) {
    m_first_dice.setTexture(m_texture_one);
  } else if (dices.first == 2) {
    m_first_dice.setTexture(m_texture_two);
  } else if (dices.first == 3) {
    m_first_dice.setTexture(m_texture_three);
  } else if (dices.first == 4) {
    m_first_dice.setTexture(m_texture_four);
  } else if (dices.first == 5) {
    m_first_dice.setTexture(m_texture_five);
  } else {
    m_first_dice.setTexture(m_texture_six);
  }

  if (dices.second == 1) {
    m_second_dice.setTexture(m_texture_one);
  } else if (dices.second == 2) {
    m_second_dice.setTexture(m_texture_two);
  } else if (dices.second == 3) {
    m_second_dice.setTexture(m_texture_three);
  } else if (dices.second == 4) {
    m_second_dice.setTexture(m_texture_four);
  } else if (dices.second == 5) {
    m_second_dice.setTexture(m_texture_five);
  } else {
    m_second_dice.setTexture(m_texture_six);
  }

  m_first_dice.setScale(0.4, 0.4);
  m_second_dice.setScale(0.4, 0.4);
  m_first_dice.setPosition(700, 450);
  m_second_dice.setPosition(760, 450);
}

} // namespace catan_game
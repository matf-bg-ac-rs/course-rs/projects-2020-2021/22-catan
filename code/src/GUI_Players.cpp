#include "../include/GUI_Players.hpp"

namespace catan_game {
/**************** Constructors/Destructor/AssignmentOperator ****************/

GUI_Players::GUI_Players(Context *context) : m_context(context) {
  m_fields = std::vector<m_player_field>(5);
  m_font = m_context->m_assets->get_font(1);

  // initialise m_trade value map to count current m_trade values
  for (int player = 1; player < m_fields.size(); ++player) {
    for (int resource = 0; resource < 5; ++resource) {
      auto card = static_cast<ResourceType>(resource);
      m_trade_values[player][card] = 0;
    }
  }
  m_player_to_trade_with = 1;
  init_fields();
}

GUI_Players::~GUI_Players() = default;

/**************** Other Methods ****************/

void GUI_Players::init_fields() {
  init_field(1);
  init_field(2);
  init_field(3);
  init_field(4);
}

void GUI_Players::init_field(int id) {
  auto players = m_context->m_assets->get_players();

  m_fields[id].m_sh.setPosition(X_ID - 3, Y_COORDS[id] - 2);
  m_fields[id].m_sh.setSize(sf::Vector2f(350, 80));
  m_fields[id].m_sh.setFillColor(sf::Color(10, 110, 255));
  m_fields[id].m_sh.setOutlineThickness(2);
  m_fields[id].m_sh.setOutlineColor(players[id]->get_color());

  m_fields[id].m_name = sf::Text(players[id]->get_name(), m_font);
  m_fields[id].m_name.setPosition(X_ID, Y_COORDS[id]);
  m_fields[id].m_name.setCharacterSize(CHAR_SIZE);
  m_fields[id].m_name.setFillColor(players[id]->get_color());

  m_fields[id].m_texture_brick =
      *m_context->m_assets->get_texture(Texture_ID::BRICK_HEX);
  m_fields[id].m_texture_stone =
      *m_context->m_assets->get_texture(Texture_ID::STONE_HEX);
  m_fields[id].m_texture_wheat =
      *m_context->m_assets->get_texture(Texture_ID::WHEAT_HEX);
  m_fields[id].m_texture_wood =
      *m_context->m_assets->get_texture(Texture_ID::WOOD_HEX);
  m_fields[id].m_texture_wool =
      *m_context->m_assets->get_texture(Texture_ID::WOOL_HEX);

  m_fields[id].m_sprite_brick.setTexture(m_fields[id].m_texture_brick);
  m_fields[id].m_sprite_stone.setTexture(m_fields[id].m_texture_stone);
  m_fields[id].m_sprite_wheat.setTexture(m_fields[id].m_texture_wheat);
  m_fields[id].m_sprite_wood.setTexture(m_fields[id].m_texture_wood);
  m_fields[id].m_sprite_wool.setTexture(m_fields[id].m_texture_wool);

  m_fields[id].m_sprite_brick.setPosition(X_ID + 1 * 75, Y_COORDS[id] + 2);
  m_fields[id].m_sprite_stone.setPosition(X_ID + 1 * 75, Y_COORDS[id] + 37);
  m_fields[id].m_sprite_wheat.setPosition(X_ID + 2 * 75, Y_COORDS[id] + 2);
  m_fields[id].m_sprite_wood.setPosition(X_ID + 2 * 75, Y_COORDS[id] + 37);
  m_fields[id].m_sprite_wool.setPosition(X_ID + 3 * 75, Y_COORDS[id] + 2);

  m_fields[id].m_sprite_brick.setScale(0.060, 0.060);
  m_fields[id].m_sprite_stone.setScale(0.060, 0.060);
  m_fields[id].m_sprite_wheat.setScale(0.060, 0.060);
  m_fields[id].m_sprite_wood.setScale(0.060, 0.060);
  m_fields[id].m_sprite_wool.setScale(0.060, 0.060);

  update_field(id);
}

void GUI_Players::draw_players() {
  for (int i = 1; i < 5; i++) {
    update_field(i);
  }

  for (int i = 0; i < m_fields.size(); i++) {
    m_context->m_window->draw(m_fields[i].m_sh);
    m_context->m_window->draw(m_fields[i].m_name);
    m_context->m_window->draw(m_fields[i].m_sprite_brick);
    m_context->m_window->draw(m_fields[i].m_sprite_stone);
    m_context->m_window->draw(m_fields[i].m_sprite_wheat);
    m_context->m_window->draw(m_fields[i].m_sprite_wood);
    m_context->m_window->draw(m_fields[i].m_sprite_wool);

    m_context->m_window->draw(m_fields[i].m_text_brick);
    m_context->m_window->draw(m_fields[i].m_text_stone);
    m_context->m_window->draw(m_fields[i].m_text_wheat);
    m_context->m_window->draw(m_fields[i].m_text_wood);
    m_context->m_window->draw(m_fields[i].m_text_wool);
    m_context->m_window->draw(m_fields[i].m_text_victory_points);
    m_context->m_window->draw(m_fields[i].m_text_longest_road);
  }
}

void GUI_Players::update_field(int id) {
  auto players = m_context->m_assets->get_players();

  m_fields[id].m_text_brick = sf::Text(players[id]->print_map_brick(), m_font);
  m_fields[id].m_text_stone = sf::Text(players[id]->print_map_stone(), m_font);
  m_fields[id].m_text_wheat = sf::Text(players[id]->print_map_wheat(), m_font);
  m_fields[id].m_text_wood = sf::Text(players[id]->print_map_wood(), m_font);
  m_fields[id].m_text_wool = sf::Text(players[id]->print_map_wool(), m_font);
  m_fields[id].m_text_victory_points =
      sf::Text(players[id]->print_victory_points(), m_font);
  m_fields[id].m_text_longest_road =
      sf::Text(players[id]->print_longest_road(), m_font);

  m_fields[id].m_text_brick.setPosition(X_ID + 110, Y_COORDS[id] + 2);
  m_fields[id].m_text_stone.setPosition(X_ID + 110, Y_COORDS[id] + 37);
  m_fields[id].m_text_wheat.setPosition(X_ID + 185, Y_COORDS[id] + 2);
  m_fields[id].m_text_wood.setPosition(X_ID + 185, Y_COORDS[id] + 37);
  m_fields[id].m_text_wool.setPosition(X_ID + 260, Y_COORDS[id] + 2);
  m_fields[id].m_text_victory_points.setPosition(X_ID + 10, Y_COORDS[id] + 25);
  m_fields[id].m_text_longest_road.setPosition(X_ID + 230, Y_COORDS[id] + 37);

  m_fields[id].m_text_brick.setFillColor(players[id]->get_color());
  m_fields[id].m_text_stone.setFillColor(players[id]->get_color());
  m_fields[id].m_text_wheat.setFillColor(players[id]->get_color());
  m_fields[id].m_text_wood.setFillColor(players[id]->get_color());
  m_fields[id].m_text_wool.setFillColor(players[id]->get_color());
  m_fields[id].m_text_victory_points.setFillColor(players[id]->get_color());
  m_fields[id].m_text_longest_road.setFillColor(players[id]->get_color());
}

void GUI_Players::react_to_click(sf::Event &e) {
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) {
    m_player_to_trade_with = 1;
  }
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) {
    m_player_to_trade_with = 2;
  }
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3)) {
    m_player_to_trade_with = 3;
  }
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4)) {
    m_player_to_trade_with = 4;
  }
  if (e.type == sf::Event::MouseButtonPressed) {
    sf::Vector2f mouse = m_context->m_window->mapPixelToCoords(
        sf::Mouse::getPosition(*(m_context->m_window)));
    for (int i = 1; i < m_fields.size(); ++i) {
      sf::FloatRect f_bounds = m_fields[i].m_sprite_brick.getGlobalBounds();
      if (f_bounds.contains(mouse)) {
        std::cout << "Player " << i << " clicked "
                  << "brick: " << ++m_trade_values[i][ResourceType::Brick]
                  << std::endl;
      }
      f_bounds = m_fields[i].m_sprite_stone.getGlobalBounds();
      if (f_bounds.contains(mouse)) {
        std::cout << "Player " << i << " clicked "
                  << "stone: " << ++m_trade_values[i][ResourceType::Stone]
                  << std::endl;
      }
      f_bounds = m_fields[i].m_sprite_wood.getGlobalBounds();
      if (f_bounds.contains(mouse)) {
        std::cout << "Player " << i << " clicked "
                  << "wood: " << ++m_trade_values[i][ResourceType::Wood]
                  << std::endl;
      }
      f_bounds = m_fields[i].m_sprite_wool.getGlobalBounds();
      if (f_bounds.contains(mouse)) {
        std::cout << "Player " << i << " clicked "
                  << "wool: " << ++m_trade_values[i][ResourceType::Wool]
                  << std::endl;
      }
      f_bounds = m_fields[i].m_sprite_wheat.getGlobalBounds();
      if (f_bounds.contains(mouse)) {
        std::cout << "Player " << i << " clicked "
                  << "wheat: " << ++m_trade_values[i][ResourceType::Wheat]
                  << std::endl;
      }
    }
  }
}

const std::map<int, std::map<ResourceType, int>> &
GUI_Players::GetMTradeValues() const {
  return m_trade_values;
}

int GUI_Players::GetMPlayerToTradeWith() const {
  return m_player_to_trade_with;
}

void GUI_Players::clear_trade_value() {
  for (int player = 1; player < m_fields.size(); ++player) {
    for (int resource = 0; resource < 5; ++resource) {
      auto card = static_cast<ResourceType>(resource);
      m_trade_values[player][card] = 0;
    }
  }
}
} // namespace catan_game

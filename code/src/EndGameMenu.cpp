//#include "../include/EndGameMenu.hpp"
//#include "../include/Loading.hpp"
//
// namespace catan_game
//{
//
//	/**************** Constructors/Destructor/AssignmentOperator
//****************/
//
//	EndGameMenu::EndGameMenu(const std::string& name, Context* context)
//		: m_context(context), m_should_return_to_loading_page(false)
//	{
//		m_font = m_context->m_assets->get_font(1);
//		m_text_end_game_message = sf::Text("Player " + name + " won the
// game.", m_font);
//
//		m_text_end_game_message.setPosition(350.f, 40.f);
//		m_text_end_game_message.setCharacterSize(35);
//		m_text_end_game_message.setFillColor(sf::Color::Yellow);
//
//		m_text_press_any_key = sf::Text("Press any key to return to main
// menu", m_font); 		m_text_press_any_key.setPosition(375.f, 90.f);
//		m_text_press_any_key.setCharacterSize(20);
//		m_text_press_any_key.setFillColor(sf::Color::Yellow);
//	}
//
//	EndGameMenu::~EndGameMenu()
//	{
//	}
//
//	/**************** Other Methods ****************/
//
//	void EndGameMenu::init()
//	{
//
//	}
//
//	void EndGameMenu::process_input()
//	{
//		sf::Event event;
//		while (m_context->m_window->pollEvent(event))
//		{
//			switch (event.type)
//			{
//				case sf::Event::Closed:
//					m_context->m_window->close();
//					break;
//				case sf::Event::KeyPressed:
//				case sf::Event::MouseButtonPressed:
//					m_should_return_to_loading_page = true;
//					break;
//			}
//		}
//	}
//
//	void EndGameMenu::update(sf::Time deltaTime)
//	{
//		if (m_should_return_to_loading_page)
//		{
//			m_context->m_states->replace_current(new
// Loading(m_context));
//		}
//	}
//
//	void EndGameMenu::draw()
//	{
//		m_context->m_window->clear(sf::Color::Black);
//		m_context->m_window->draw(m_text_end_game_message);
//		m_context->m_window->draw(m_text_press_any_key);
//		m_context->m_window->display();
//	}
//
//}
//
//

#include "../include/EndGameMenu.hpp"
#include "../include/Loading.hpp"

namespace catan_game {
EndGameMenu::EndGameMenu(const std::string &name, Context *context)
    : m_context(context), m_should_return_to_loading_page(false) {
  m_font = m_context->m_assets->get_font(1);
  m_text_end_game_message =
      sf::Text("Player " + name + " won the game.", m_font);

  m_text_end_game_message.setPosition(380.f, 580.f);
  m_text_end_game_message.setCharacterSize(25);
  m_text_end_game_message.setFillColor(sf::Color::Black);
  if (!m_texture_background.loadFromFile("./../resources/images/pehar.png")) {
    std::cout << __FILE__ << " "
              << "Failed loading background image" << std::endl;
    exit(EXIT_FAILURE);
  }
  m_background_image.setTexture(m_texture_background);

  m_text_press_any_key =
      sf::Text("Press any key to return to main menu", m_font);
  m_text_press_any_key.setPosition(380.f, 610.f);
  m_text_press_any_key.setCharacterSize(15);
  m_text_press_any_key.setFillColor(sf::Color::Black);
}

EndGameMenu::~EndGameMenu() {}

void EndGameMenu::init() {}

void EndGameMenu::process_input() {
  sf::Event event;
  while (m_context->m_window->pollEvent(event)) {
    switch (event.type) {
    case sf::Event::Closed:
      m_context->m_window->close();
      break;
    case sf::Event::KeyPressed:
    case sf::Event::MouseButtonPressed:
      m_should_return_to_loading_page = true;
      break;
    }
  }
}

void EndGameMenu::update(sf::Time deltaTime) {
  if (m_should_return_to_loading_page) {
    m_context->m_states->replace_current(new Loading(m_context));
  }
}

void EndGameMenu::draw() {
  m_context->m_window->clear(sf::Color(30, 144, 255));
  m_context->m_window->draw(m_background_image);
  m_context->m_window->draw(m_text_end_game_message);
  m_context->m_window->draw(m_text_press_any_key);
  m_context->m_window->display();
}

} // namespace catan_game

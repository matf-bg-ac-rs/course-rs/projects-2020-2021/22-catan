#include <iostream>
#include <string>
#include <vector>

#include "SFML/Graphics.hpp"

#include "../include/GameRoom.hpp"
#include "../include/Menu.hpp"

namespace catan_game {
/**************** Constructors/Destructor/AssignmentOperator ****************/

Menu::Menu(Context *context)
    : m_context(context), m_has_selected_field(false), m_input_finished(false),
      m_selected_field(-1) {
  // VAZNO!!!
  // Pravimo pet polja, ali se nulto polje uvek preskace, to radimo da bismo
  // poljima redom pristupali sa 1,2,3,4
  m_fields = std::vector<m_player_field>(5);
  m_players = std::vector<Player *>(5);
}

Menu::~Menu() {}

/**************** Other Methods ****************/

void Menu::init() {
  if (!m_texture_board.loadFromFile("../resources/images/board.png")) {
    std::cout << __FILE__ << " "
              << "Failed loading board image" << std::endl;
    exit(EXIT_FAILURE);
  }
  m_board_image.setTexture(m_texture_board);
  m_board_image.setOrigin(m_board_image.getLocalBounds().width / 2,
                          m_board_image.getLocalBounds().height / 2);
  m_board_image.setPosition(m_context->m_window->getSize().x / 2 - 250,
                            m_context->m_window->getSize().y / 2);

  m_context->m_assets->add_font(1, "../resources/font/glue_gun.ttf");
  m_font = m_context->m_assets->get_font(1);

  init_fields();
}

void Menu::process_input() {
  sf::Event event;
  while (m_context->m_window->pollEvent(event)) {
    if (event.type == sf::Event::Closed) {
      m_context->m_window->close();
    }
    if (event.mouseButton.button == sf::Mouse::Left) {
      sf::Vector2f mouse = m_context->m_window->mapPixelToCoords(
          sf::Mouse::getPosition(*(m_context->m_window)));
      m_selected_field = get_clicked_field(mouse);

      if (m_selected_field != -1) {
        select_field(m_selected_field);
        m_has_selected_field = true;
      } else {
        unselect_field();
        m_has_selected_field = false;
      }
    }
    if (event.type == sf::Event::TextEntered) {
      if (m_has_selected_field && event.text.unicode < 128) {
        char c = static_cast<char>(event.text.unicode);
        read_name(m_selected_field, c);
      }
    }
    if (event.type == sf::Event::KeyPressed) {
      if (event.key.code == sf::Keyboard::Enter) {
        is_input_finished();
      }
    }
  }
}

void Menu::update(sf::Time deltaTime) {
  if (m_input_finished) {
    std::string name1 = m_fields[1].m_input_name.toAnsiString();
    std::string name2 = m_fields[2].m_input_name.toAnsiString();
    std::string name3 = m_fields[3].m_input_name.toAnsiString();
    std::string name4 = m_fields[4].m_input_name.toAnsiString();
    m_players[1] = new Player(1, name1, m_fields[1].m_id.getFillColor());
    m_players[2] = new Player(2, name2, m_fields[2].m_id.getFillColor());
    m_players[3] = new Player(3, name3, m_fields[3].m_id.getFillColor());
    m_players[4] = new Player(4, name4, m_fields[4].m_id.getFillColor());
    m_context->m_assets->create_players(m_players);

    m_context->m_states->replace_current(new GameRoom(m_context));
  }
}

void Menu::draw() {
  m_context->m_window->clear(sf::Color(30, 144, 255));
  m_context->m_window->draw(m_board_image);
  draw_menu();
  m_context->m_window->display();
}

void Menu::init_fields() {
  init_header(1, "P1", sf::Color::Blue, sf::Vector2i(X_ID, Y1));
  init_header(2, "P2", sf::Color::Red, sf::Vector2i(X_ID, Y2));
  init_header(3, "P3", sf::Color::Green, sf::Vector2i(X_ID, Y3));
  init_header(4, "P4", sf::Color(255, 140, 0), sf::Vector2i(X_ID, Y4));

  init_label(1, sf::Vector2i(X_LABEL, Y1));
  init_label(2, sf::Vector2i(X_LABEL, Y2));
  init_label(3, sf::Vector2i(X_LABEL, Y3));
  init_label(4, sf::Vector2i(X_LABEL, Y4));
}

void Menu::init_header(int id, const std::string &name, sf::Color color,
                       sf::Vector2i pos) {
  m_fields[id].m_id.setString(name);
  m_fields[id].m_id.setFillColor(color);
  m_fields[id].m_id.setPosition(pos.x, pos.y);
  m_fields[id].m_id.setStyle(sf::Text::Bold);
  m_fields[id].m_id.setFont(m_font);
  m_fields[id].m_id.setCharacterSize(CHAR_SIZE);
}

void Menu::init_label(int id, sf::Vector2i pos) {
  m_fields[id].m_label_name.setPosition(pos.x, pos.y);
  m_fields[id].m_label_name.setSize(LABEL_SIZE);
  m_fields[id].m_label_name.setOutlineColor(GREY);
  m_fields[id].m_label_name.setOutlineThickness(5);
  m_fields[id].m_label_name.setFillColor(sf::Color::White);

  const int x = m_fields[id].m_label_name.getPosition().x;
  const int y = m_fields[id].m_label_name.getPosition().y;
  sf::Color color = m_fields[id].m_id.getFillColor();
  m_fields[id].m_name.setFont(m_font);
  m_fields[id].m_name.setPosition(x + 5, y + 5);
  m_fields[id].m_name.setFillColor(color);
}

int Menu::get_clicked_field(sf::Vector2f mouse) {
  for (int i = 1; i < m_fields.size(); i++) {
    sf::FloatRect bounds = m_fields[i].m_label_name.getGlobalBounds();
    if (bounds.contains(mouse)) {
      return i;
    }
  }
  return -1;
}

void Menu::select_field(int num_field) {
  for (int i = 1; i < m_fields.size(); i++) {
    if (i == num_field) {
      sf::Color color = m_fields[i].m_id.getFillColor();
      m_fields[i].m_label_name.setOutlineColor(color);
    } else {
      m_fields[i].m_label_name.setOutlineColor(GREY);
    }
  }
}

void Menu::unselect_field() {
  for (int i = 1; i < m_fields.size(); i++) {
    m_fields[i].m_label_name.setOutlineColor(GREY);
  }
}

void Menu::read_name(int id, char c) {
  if (m_fields[id].m_input_name.getSize() < 8) {
    m_fields[id].m_input_name += c;
    m_fields[id].m_name.setString(m_fields[id].m_input_name);
  }
}

void Menu::draw_menu() {
  m_context->m_window->draw(m_fields[1].m_id);
  m_context->m_window->draw(m_fields[2].m_id);
  m_context->m_window->draw(m_fields[3].m_id);
  m_context->m_window->draw(m_fields[4].m_id);
  m_context->m_window->draw(m_fields[1].m_label_name);
  m_context->m_window->draw(m_fields[2].m_label_name);
  m_context->m_window->draw(m_fields[3].m_label_name);
  m_context->m_window->draw(m_fields[4].m_label_name);
  m_context->m_window->draw(m_fields[1].m_name);
  m_context->m_window->draw(m_fields[2].m_name);
  m_context->m_window->draw(m_fields[3].m_name);
  m_context->m_window->draw(m_fields[4].m_name);
}

void Menu::is_input_finished() {
  for (int i = 1; i < m_fields.size(); i++) {
    if (m_fields[i].m_input_name.getSize() < 3) {
      return;
    }
  }
  m_input_finished = true;
}

} // namespace catan_game
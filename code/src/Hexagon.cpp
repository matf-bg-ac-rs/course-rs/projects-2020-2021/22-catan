#include <iostream>

#include "../include/Hexagon.hpp"

namespace catan_game {
/**************** Constructors/Destructor/AssignmentOperator ****************/

Hexagon::Hexagon(int id, Node *left_up_node, Node *up_node, Node *right_up_node,
                 Node *right_bottom_node, Node *bottom_node,
                 Node *left_bottom_node, Road *up_left_road,
                 Road *up_right_road, Road *right_road, Road *down_right_road,
                 Road *down_left_road, Road *left_road, ResourceType type,
                 int number)
    : m_id(id), m_left_up_node(left_up_node), m_up_node(up_node),
      m_right_up_node(right_up_node), m_right_bottom_node(right_bottom_node),
      m_bottom_node(bottom_node), m_left_bottom_node(left_bottom_node),
      m_up_left_road(up_left_road), m_up_right_road(up_right_road),
      m_right_road(right_road), m_down_right_road(down_right_road),
      m_down_left_road(down_left_road), m_left_road(left_road), m_type(type),
      m_number(number), m_is_robber_here(false) {
  m_vertices = {m_left_up_node,      m_up_node,     m_right_up_node,
                m_right_bottom_node, m_bottom_node, m_left_bottom_node};
  m_roads = {m_up_left_road,    m_up_right_road,  m_right_road,
             m_down_right_road, m_down_left_road, m_left_road};
  std::tie(m_row, m_column) = calculate_row_and_column();
}

/**************** Getters ****************/

int Hexagon::get_id() const { return m_id; }

const std::vector<Node *> &Hexagon::get_nodes() const { return m_vertices; }

Node *Hexagon::get_left_up_node() const { return m_left_up_node; }

Node *Hexagon::get_up_node() const { return m_up_node; }

Node *Hexagon::get_right_up_node() const { return m_right_up_node; }

Node *Hexagon::get_right_bottom_node() const { return m_right_bottom_node; }

Node *Hexagon::get_bottom_node() const { return m_bottom_node; }

Node *Hexagon::get_left_bottom_node() const { return m_left_bottom_node; }

const std::vector<Road *> &Hexagon::get_roads() const { return m_roads; }

Road *Hexagon::get_up_left_road() const { return m_up_left_road; }

Road *Hexagon::get_up_right_road() const { return m_up_right_road; }

Road *Hexagon::get_right_road() const { return m_right_road; }

Road *Hexagon::get_down_right_road() const { return m_down_right_road; }

Road *Hexagon::get_down_left_road() const { return m_down_left_road; }

Road *Hexagon::get_left_road() const { return m_left_road; }

ResourceType Hexagon::get_type() const { return m_type; }

int Hexagon::get_number() const { return m_number; }

bool Hexagon::is_robber_here() const { return m_is_robber_here; }

int Hexagon::get_row() const { return m_row; }

int Hexagon::get_column() const { return m_column; }

sf::Vector2f Hexagon::get_center() { return m_center; }

/**************** Setters ****************/

void Hexagon::set_is_robber_here(bool value) { m_is_robber_here = value; }

void Hexagon::set_center(sf::Vector2f center) { m_center = center; }

bool Hexagon::is_clicked(sf::Vector2f pos) {
  int diff = 30;
  bool hor = m_center.x - diff < pos.x && pos.x < m_center.x + diff;
  bool ver = m_center.y - diff < pos.y && pos.y < m_center.y + diff;
  if (hor && ver) {
    return true;
  } else {
    return false;
  }
}

/**************** Other Methods ****************/

std::pair<int, int> Hexagon::calculate_row_and_column() const {
  int row;
  int column;
  switch (m_id) {
  case 0:
    row = 0;
    column = 0;
    break;
  case 1:
    row = 0;
    column = 1;
    break;
  case 2:
    row = 0;
    column = 2;
    break;
  case 3:
    row = 1;
    column = 0;
    break;
  case 4:
    row = 1;
    column = 1;
    break;
  case 5:
    row = 1;
    column = 2;
    break;
  case 6:
    row = 1;
    column = 3;
    break;
  case 7:
    row = 2;
    column = 0;
    break;
  case 8:
    row = 2;
    column = 1;
    break;
  case 9:
    row = 2;
    column = 2;
    break;
  case 10:
    row = 2;
    column = 3;
    break;
  case 11:
    row = 2;
    column = 4;
    break;
  case 12:
    row = 3;
    column = 0;
    break;
  case 13:
    row = 3;
    column = 1;
    break;
  case 14:
    row = 3;
    column = 2;
    break;
  case 15:
    row = 3;
    column = 3;
    break;
  case 16:
    row = 4;
    column = 0;
    break;
  case 17:
    row = 4;
    column = 1;
    break;
  case 18:
    row = 4;
    column = 2;
    break;
  default:
    std::cout << "greska Hexagon::calculate_row_and_column" << std::endl;
    std::exit(EXIT_FAILURE);
  }
  return std::make_pair(row, column);
}

} // namespace catan_game
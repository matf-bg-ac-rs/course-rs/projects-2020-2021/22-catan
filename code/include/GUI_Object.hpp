#ifndef CATAN_GAME_GUI_OBJECT_HPP
#define CATAN_GAME_GUI_OBJECT_HPP

#include <SFML/System.hpp>

namespace catan_game {
class GUI_Object {
public:
  /**************** Constructors/Destructor/AssignmentOperator ****************/
  GUI_Object(float center_x, float center_y);

  virtual ~GUI_Object() {}

  GUI_Object(const GUI_Object &) = delete;
  GUI_Object &operator=(const GUI_Object &) = delete;

  /**************** Other Methods ****************/
  virtual void draw() = 0;

protected:
  /**************** Attributes ****************/
  sf::Vector2f m_center;
};

} // namespace catan_game

#endif // CATAN_GAME_GUI_OBJECT_HPP
#ifndef CATAN_GAME_GUI_SPRITE_OBJECT_HPP
#define CATAN_GAME_GUI_SPRITE_OBJECT_HPP

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "GUI_Object.hpp"
#include "Game.hpp"

namespace catan_game {
class GUI_Sprite_Object : public GUI_Object {
public:
  /**************** Constructors/Destructor/AssignmentOperator ****************/
  GUI_Sprite_Object(float center_x, float center_y, sf::Texture *texture,
                    Context *context);
  GUI_Sprite_Object(const GUI_Sprite_Object &) = delete;
  GUI_Sprite_Object &operator=(const GUI_Sprite_Object &) = delete;

  /**************** Other Methods ****************/
  virtual void draw() override;
  void set_origin_to_center();
  void set_scale(float x_scaling_factor, float y_scaling_factor);
  void place_inside(GUI_Sprite_Object *other, float offset_x, float offset_y);
  void set_position(float x, float y);

protected:
  /**************** Attributes ****************/
  sf::Sprite m_sprite;
  sf::Texture *m_texture;
  Context *m_context;
};

} // namespace catan_game

#endif // CATAN_GAME_GUI_SPRITE_OBJECT_HPP

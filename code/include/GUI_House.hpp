#ifndef CATAN_GAME_CODE_INCLUDE_GUI_HOUSE_HPP
#define CATAN_GAME_CODE_INCLUDE_GUI_HOUSE_HPP

#include "GUI_Sprite_Object.hpp"

namespace catan_game {
class GUI_House : public GUI_Sprite_Object {
public:
  /**************** Constructors/Destructor/AssignmentOperator ****************/
  GUI_House(float center_x, float center_y, Owners owner, Context *context);
  GUI_House(const GUI_House &) = delete;
  GUI_House &operator=(const GUI_House &) = delete;

private:
  /**************** Other Methods ****************/
  Texture_ID owners_to_texture_id(Owners owner);
};

} // namespace catan_game

#endif // CATAN_GAME_CODE_INCLUDE_GUI_HOUSE_HPP

#ifndef CATAN_GAME_CODE_INCLUDE_GUI_CITY_HPP
#define CATAN_GAME_CODE_INCLUDE_GUI_CITY_HPP

#include "GUI_Sprite_Object.hpp"

namespace catan_game {
class GUI_City : public GUI_Sprite_Object {
public:
  /**************** Constructors/Destructor/AssignmentOperator ****************/
  GUI_City(float center_x, float center_y, Owners owner, Context *context);
  GUI_City(const GUI_City &) = delete;
  GUI_City &operator=(const GUI_City &) = delete;

private:
  /**************** Other Methods ****************/
  Texture_ID owners_to_texture_id(Owners owner);
};

} // namespace catan_game

#endif // CATAN_GAME_CODE_INCLUDE_GUI_CITY_HPP

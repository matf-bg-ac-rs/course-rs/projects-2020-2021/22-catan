#ifndef CATAN_GAME_CODE_SRC_TRADE_HPP
#define CATAN_GAME_CODE_SRC_TRADE_HPP

#include <iostream>

#include <SFML/Graphics.hpp>

#include "../include/Game.hpp"
#include "GUI_Players.hpp"

namespace catan_game {
class Trade {
public:
  Trade(Context *context, GUI_Players *gui_players);
  void trade(int m_turn);
  bool is_trade_clicked(sf::Vector2f mouse);
  void draw(std::vector<Player *> players);
  bool is_trade_reset_clicked(sf::Vector2f mouse);

private:
  Context *m_context;
  GUI_Players *m_gui_players;
  sf::Text m_trade_reset;
  sf::Text m_trade_button;
};
} // namespace catan_game

#endif // CATAN_GAME_CODE_SRC_TRADE_HPP

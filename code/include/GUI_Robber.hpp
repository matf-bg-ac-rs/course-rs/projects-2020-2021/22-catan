#ifndef CATAN_GAME_CODE_INCLUDE_GUI_ROBBER_HPP
#define CATAN_GAME_CODE_INCLUDE_GUI_ROBBER_HPP

#include "GUI_Sprite_Object.hpp"

namespace catan_game {
class GUI_Robber : public GUI_Sprite_Object {
public:
  /**************** Constructors/Destructor/AssignmentOperator ****************/
  GUI_Robber(float center_x, float center_y, Texture_ID texture_id,
             Context *context);
  GUI_Robber(const GUI_Robber &) = delete;
  GUI_Robber &operator=(const GUI_Robber &) = delete;
};

} // namespace catan_game

#endif // CATAN_GAME_CODE_INCLUDE_GUI_ROBBER_HPP

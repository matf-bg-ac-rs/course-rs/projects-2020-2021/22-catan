#ifndef CATAN_GAME_OWNERS_HPP
#define CATAN_GAME_OWNERS_HPP

namespace catan_game {
enum class Owners { Player1, Player2, Player3, Player4, NoOwner };
}

#endif // CATAN_GAME_OWNERS_HPP
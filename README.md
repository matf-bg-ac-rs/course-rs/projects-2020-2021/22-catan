[![cpp](https://img.shields.io/badge/Language-C%2B%2B-blue?style=flat-square)](https://www.cplusplus.com/)

# Project 22-Catan

Imitation of the game "The Settlers of Catan" implemented for Software development course.

## Prerequisites

SFML: <https://www.sfml-dev.org>
```shell
sudo apt-get install libsfml-dev
```

CMAKE: <https://cmake.org/>

CLION: <https://www.jetbrains.com/clion/>

## 🔨 Building

Preferred way of building our project is through the Clion IDE, you just need to clone this repository first:
```shell
git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/22-catan.git
```

Then position to the directory you just made and open Clion in it:
```shell
cd 22-catan
clion .
```

Clion should automatically load CMakeLists.txt and you are good to go! If you have any troubles, feel free to contact us.


## 🎮 Gameplay
The game follows the official "The Settlers of Catan" rules(https://www.catan.com/service/game-rules). In order to trade resource cards with players 1-4 you first need to choose who you want to trade with by pressing numbers 
1️⃣-4️⃣ on your keyboard and then you need to click 
on resources you want to give and take as many times as the number of cards you want to give and take. Then simply click on _Trade with_ text. In order to place a house, road or city(after initial round) by standard prices you need to click on the appropriate icon and place it on the appropriate position. Longest road points are calculated in every turn and displayed as LR value. 🏆 points are displayed below every player(you need to achieve 10 of those in order to win).

## 🖥️ Demo
Video: https://www.youtube.com/watch?v=HdZgSipHNsE

Weekly reports(in Serbian): https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/22-catan/-/wikis/Izve%C5%A1taji

## Developers

- [Teodora Isailovic, 240/2017](https://gitlab.com/teodora_isailovic)
- [Mihailo Vlajkovic, 247/2017](https://gitlab.com/hamagrid)
- [Filip Filipovic, 113/2017](https://gitlab.com/ffilipovicc98)
- [Boris Cvitak, 161/2017](https://gitlab.com/cb___)
